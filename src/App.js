import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Shoes_hook from "./Shoes_hook/Shoes_Hook";

function App() {
  return (
    <>
      <BrowserRouter>
        <Route exact path="/" component={Shoes_hook} />
      </BrowserRouter>
    </>
  );
}

export default App;
