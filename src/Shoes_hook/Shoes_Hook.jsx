import React, { useEffect, useState } from "react";
import GioHang from "./GioHang";
import ListSanPham from "./ListSanPham";
import { data_shoes } from "./data";

export default function Shoes_hook() {
  let [data, setData] = useState(data_shoes);
  let [gioHang, setGioHang] = useState([]);
  let handleAddToCart = (shoe) => {
    let index = gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneGioHang = [...gioHang];

    if (index == -1) {
      let newSp = { ...shoe, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      // th2 trong giỏi hàng đã có sản phẩm
      cloneGioHang[index].soLuong++;
    }
    setGioHang((gioHang = cloneGioHang));
  };
  let handleChangeQuantity = (idShoe, value) => {
    let index = gioHang.findIndex((shoe) => {
      return shoe.id == idShoe;
    });

    if (index == -1) return;

    let cloneGioHang = [...gioHang];

    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + value;

    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    setGioHang((gioHang = cloneGioHang));
  };
  let handleDelete = (idShoe) => {
    let index = gioHang.findIndex((shoe) => {
      return shoe.id == idShoe;
    });
    let cloneGioHang = [...gioHang];
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
    }

    setGioHang((gioHang = cloneGioHang));
  };
  let renderSanPham = () => {
    return data.map((item, index) => {
      return (
        <div className="col-3" key={index}>
          <ListSanPham handleAddToCart={handleAddToCart} dataSanPham={item} />
        </div>
      );
    });
  };
  return (
    <div className="container">
      <GioHang
        handleDelete={handleDelete}
        handleChangeQuantity={handleChangeQuantity}
        dataGioHang={gioHang}
      />
      <div className="row">{renderSanPham()}</div>
      <br />
    </div>
  );
}
