import React, { memo, useState } from "react";
import "./shoeHook.css";

export default function ListSanPham({ dataSanPham, handleAddToCart }) {
  return (
    <div className="p-10 item rounded-lg">
      <img src={dataSanPham.image} alt="" width="100%" />
      <div className="item__desc flex">
        <div className="left">
          <h2 style={{ height: "80px" }}>{dataSanPham.name}</h2>
          <p>Quantity: {dataSanPham.quantity}</p>
          <div>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
          </div>
        </div>
        <div className="right">
          <h2>{dataSanPham.price}$</h2>
          <button
            onClick={() => {
              handleAddToCart(dataSanPham);
            }}
            className="btn btn-outline-dark"
          >
            Buy now
          </button>
        </div>
      </div>
    </div>
  );
}
