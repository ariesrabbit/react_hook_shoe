import React from "react";
import "./shoeHook.css";

export default function GioHang({
  dataGioHang,
  handleChangeQuantity,
  handleDelete,
}) {
  let renderTbody = () => {
    return dataGioHang.map((itemGioHang, index) => {
      return (
        <>
          <tr key={index}>
            <th scope="row">{index + 1}</th>
            <td>{itemGioHang.name}</td>
            <td>{itemGioHang.soLuong}</td>
            <td>{itemGioHang.soLuong * itemGioHang.price}$</td>
            <td>
              <div>
                <button
                  onClick={() => {
                    handleChangeQuantity(itemGioHang.id, 1);
                  }}
                  className="btn btn-outline-success px-3 fw-semibold mx-1"
                >
                  +
                </button>

                <button
                  onClick={() => {
                    handleChangeQuantity(itemGioHang.id, -1);
                  }}
                  className="btn btn-outline-secondary px-3 fw-semibold mx-1"
                >
                  -
                </button>

                <button
                  onClick={() => {
                    handleDelete(itemGioHang.id);
                  }}
                  className="btn btn-outline-danger px-2 fw-semibold mx-1"
                >
                  <i className="fa fa-trash-alt" />
                </button>
              </div>
            </td>
          </tr>
        </>
      );
    });
  };
  return (
    <div>
      <table className="table table-striped w-75 text-center m-auto">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Tên sản phẩm</th>
            <th scope="col">Số lượng</th>
            <th scope="col">Giá tiền</th>
            <th scope="col">Tăng /Giảm /Xóa</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
